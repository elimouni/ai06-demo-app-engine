//require('zone.js/dist/zone-node');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.disable('x-powered-by');


app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token,authorization');

  console.log(`${new Date().toString()} => ${req.originalUrl}`);
  next();
});

app.get("/double_return", (req, res) => {
  for (var i = 10; i >= 0; i--) {
    res.status(200).jsonp('It worked !');
  }
});

app.get('/no_id_provided', (req, res) => {
    validationResult(req).throw();
    res.status(200).jsonp({msg:req.body.id});
});

app.get('/env_var', (req, res) => {
    res.status(200).jsonp({user:process.env.DB_USER, pass: process.env.DB_PASS, name: process.env.DB_NAME});
});

app.use('/', express.static(path.join(__dirname, 'public')));
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

//All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req });
});

app.use((req, res, next) => {
  res.status(404).jsonp('Unauthorized request!')
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).jsonp('Internal server error!');
});

app.listen(8080, () => {
  console.log('server started port:8080');
});
